*** Settings ***
Suite Teardown    Close All Browsers
Library           SeleniumLibrary
Library           BuiltIn
Library           String
Library           Screenshot

*** Variables ***
${url}            https://www.google.com
${word}           PTT Digital
${title}          results

*** Test Cases ***
Search
    GotoUrl
    InputWord
    SearchResults
    Screenshot

*** Keywords ***
GotoUrl
    Open Browser    about:blank    chrome    options=add_argument('--ignore-certificate-errors');add_argument('--disable-blink-features="BlockCredentialedSubresources"')
    Maximize Browser Window
    Go To    ${url}

InputWord
    Input Text    //*[@name="q"]    ${word}

SearchResults
    Click Button    //*[@name="btnK"]

Screenshot
    Screenshot.Set Screenshot Directory    ${CURDIR}${/}Screenshot
    Take Screenshot
