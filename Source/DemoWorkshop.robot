*** Settings ***
Suite Teardown    Close All Browsers
Library           SeleniumLibrary
Library           BuiltIn
Library           String
Library           Screenshot

*** Variables ***
${url}            https://ptt-devpool-robotframework.herokuapp.com/

*** Test Cases ***
Login - Success
    [Tags]    Success
    GotoUrl
    Signin    admin    1234
    Title Should Be    DevPool - RobotFramework : application
    Screenshot.Set Screenshot Directory    ${CURDIR}${/}Screenshot
    Take Screenshot    LoginSuccess.jpg

Login - Fail
    [Tags]    Fail
    GotoUrl
    Signin    test    test
    ${alert}=    Handle Alert    action=ACCEPT
    BuiltIn.Should Be Equal    ${alert}    Invalid Username or Password
    Screenshot.Set Screenshot Directory    ${CURDIR}${/}Screenshot
    Take Screenshot    LoginFail.jpg

New Data
    GotoUrl
    Signin    admin    1234
    Title Should Be    DevPool - RobotFramework : application
    Click Button    //*[@id="btn_new"]
    Input Text    //*[@id="txt_new_firstname"]    Naruephon
    Input Text    //*[@id="txt_new_lastname"]    Serikitkankul
    Input Text    //*[@id="txt_new_email"]    znaruephon.s@pttdigital.com
    Click Button    //*[@id="btn_new_save"]
    Page Should Contain    Naruephon
    Screenshot.Set Screenshot Directory    ${CURDIR}${/}Screenshot
    Take Screenshot    NewData.jpg

*** Keywords ***
GotoUrl
    Open Browser    about:blank    chrome    options=add_argument('--ignore-certificate-errors');add_argument('--disable-blink-features="BlockCredentialedSubresources"')
    Go To    ${url}
    Maximize Browser Window

Signin
    [Arguments]    ${username}    ${password}
    Input Text    //*[@id="txt_username"]    ${username}
    Input Text    //*[@id="txt_password"]    ${password}
    Click Button    //*[@id="btn_login"]
