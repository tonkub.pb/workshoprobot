*** Settings ***
Suite Teardown    Close All Browsers
Library           SeleniumLibrary
Library           BuiltIn
Library           String
Library           Screenshot
Library           ExcelLibrary

*** Variables ***
${url}            https://ptt-devpool-robotframework.herokuapp.com/

*** Test Cases ***
Login - Success
    [Tags]    Success
    GotoUrl
    Signin    admin    1234
    Title Should Be    DevPool - RobotFramework : application
    Screenshot.Set Screenshot Directory    ${CURDIR}${/}Screenshot
    Take Screenshot    LoginSuccess.jpg

Login - Fail
    [Tags]    Fail
    GotoUrl
    Signin    test    test
    ${alert}=    Handle Alert    action=ACCEPT
    BuiltIn.Should Be Equal    ${alert}    Invalid Username or Password
    Screenshot.Set Screenshot Directory    ${CURDIR}${/}Screenshot
    Take Screenshot    LoginFail.jpg

New Data
    GotoUrl
    Signin    admin    1234
    Title Should Be    DevPool - RobotFramework : application
    Open Excel Document    ${CURDIR}${/}DataTest.xlsx    doc_id=docid
    ${sheet}=    Get Sheet    sheet_name=Sheet1
    FOR    ${index}    IN RANGE    2    4
        ${firstname}=    Read Excel Cell    row_num=${index}    col_num=1
        ${lastname}=    Read Excel Cell    row_num=${index}    col_num=2
        ${email}=    Read Excel Cell    row_num=${index}    col_num=3
        Click Button    //*[@id="btn_new"]
        Input Text    //*[@id="txt_new_firstname"]    ${firstname}
        Input Text    //*[@id="txt_new_lastname"]    ${lastname}
        Input Text    //*[@id="txt_new_email"]    ${email}
        Click Button    //*[@id="btn_new_save"]
    END
    Screenshot.Set Screenshot Directory    ${CURDIR}${/}Screenshot
    Take Screenshot    NewData.jpg

*** Keywords ***
GotoUrl
    Open Browser    about:blank    chrome    options=add_argument('--ignore-certificate-errors');add_argument('--disable-blink-features="BlockCredentialedSubresources"')
    Go To    ${url}
    Maximize Browser Window

Signin
    [Arguments]    ${username}    ${password}
    Input Text    //*[@id="txt_username"]    ${username}
    Input Text    //*[@id="txt_password"]    ${password}
    Click Button    //*[@id="btn_login"]
