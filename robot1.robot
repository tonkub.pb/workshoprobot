*** Settings ***
Library  BuiltIn

*** Variables ***
${pi}=  3.14
${radius}=  7
${area}=  0

*** Test Cases ***
1. คำนวณพื้นที่วงกลม
    ${area}=  CircleArea  ${pi}  ${radius}
    Set Global Variable  ${area}
    
2. แสดงผล
    Log To Console  \nArea: ${area}



*** Keywords ***
CircleArea
    [Arguments]  ${parapi}  ${paramradius}
    ${respArea}=  Evaluate  ${parapi} * ${paramradius} * ${paramradius}
    [Return]  ${respArea}

